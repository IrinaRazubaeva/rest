package com.example.test_rest.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table (name = "web_page")
public class WebPage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "url")
    private String url;

    @OneToMany(mappedBy = "webPage", cascade = CascadeType.ALL, orphanRemoval = true)
    List<WebWord> words = new ArrayList<>();

    public WebPage(){
    }

    public WebPage(String url){
        this.url = url;
        words = new ArrayList<>();
    }

    public void addWord(WebWord word) {
        word.setWebPage(this);
        words.add(word);
    }

    public int getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<WebWord> getWebWord() {
        return words;
    }

    public void setWebWord(List<WebWord> words) {
        this.words = words;
    }

}
