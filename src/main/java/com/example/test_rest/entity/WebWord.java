package com.example.test_rest.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "words")
public class WebWord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "word")
    private String name;

    @Column(name = "cnt")
    private int cnt;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "web_page_id")
    private WebPage webPage;

    public WebWord(){

    }

    public WebWord(String name, int cnt){
        this.name = name;
        this.cnt = cnt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    @JsonIgnore
    public WebPage getWebPage() {
        return webPage;
    }

    public void setWebPage(WebPage webPage) {
        this.webPage = webPage;
    }
}
