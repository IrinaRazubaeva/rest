package com.example.test_rest.service;

import com.example.test_rest.dao.WordDAO;

import com.example.test_rest.entity.WebPage;
import com.example.test_rest.entity.WebWord;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class Web {

    @Autowired
    private WordDAO wordDAO;

    private String nameWeb;
    private String titleUrl;
    private String bodyUrl;
    private ArrayList<String> listBody;
    //WordDAOImpl wordDaoImpl = new WordDAOImpl();

    public Web() {
    }

    public Web(String nameWeb) {
        this.nameWeb = nameWeb;
    }

    public void setNameWeb(String nameWeb) {
        this.nameWeb = nameWeb;
    }

    public WebPage getWebPage() {
        return getDB();
    }

    public WebPage getDB() {
        WebPage wordId = new WebPage();
        wordId = wordDAO.findByUrl(nameWeb);
        if (wordId == null) {
            wordId = new WebPage();
            wordId.setUrl(nameWeb);
            wordDAO.save(wordId);
            //temp(wordId);
            while (!getHtml()) {
                System.out.println("-");
            }

            calculate();
            add(wordId);

        }
        return wordId;
    }

    public void temp(WebPage webPage) {
        WebWord word = new WebWord("Bye", 4);
        //word.setWebPage(webPage);
        webPage.addWord(word);
        WebWord word1 = new WebWord("world", 7);
        //word1.setWebPage(webPage);
        webPage.addWord(word1);
        wordDAO.save(webPage);

    }

    /**
     * Метод, в котором происходит загрузка заголовка и текста страницы
     */
    public boolean getHtml() {
        Document document = null;
        try {
            document = Jsoup.connect(nameWeb).get();
            titleUrl = document.title();
            bodyUrl = document.text();
            System.out.println("Загрузка страницы - успешено");

        } catch (Exception e) {
            System.out.println(e.getMessage() + " Неудачное соединение или адрес введен неверно");
            return false;
        }
        return true;
    }

    /**
     * Метод, в котором создается переменная типа "ArrayList". В ней хранятся все слова текста страницы
     */
    public void calculate() {
        if (titleUrl.equals("error")) {
            return;
        }
        String[] split = bodyUrl.split("[\\s \",.?!;:$&%\'[ ](){}/<>\n\r\t]");
        listBody = new ArrayList<String>(Arrays.asList(split));
        String[] symbol = new String[]{"", "*", "**", "+", "-", "="};
        Collections.sort(listBody);
        for (String del : symbol) {
            removeTrash(del, listBody);
        }

    }

    /**
     * Метод, в котором происходит удаление указанного слова (или слов, если оно повторяется) из указанного списка
     *
     * @param str  - переменная, хранящее слово, которое нужно удалить
     * @param list - список, из которого нужно удалить слово
     */
    public void removeTrash(String str, ArrayList<String> list) {
        int index = 0;
        while (list.contains(str)) {
            index = list.indexOf(str);
            list.remove(index);
        }
    }

    /**
     * Метод, в котором данные из переменной типа "ArrayList" записываются в базу данных
     */
    public void add(WebPage webPage) {
        String name;
        int cnt = 0;
        //WebPage webPage = new WebPage();
        //webPage.setUrl(nameWeb);
        while (listBody.size() > 0) {
            name = listBody.get(0);
            int index = 0;
            cnt = 0;
            while (listBody.contains(name)) {
                index = listBody.indexOf(name);
                listBody.remove(index);
                cnt++;
            }
            WebWord word = new WebWord(name, cnt);
            webPage.addWord(word);
        }
        wordDAO.save(webPage);
    }

}
