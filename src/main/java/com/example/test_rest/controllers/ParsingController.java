package com.example.test_rest.controllers;

import com.example.test_rest.entity.WebPage;
import com.example.test_rest.entity.WebWord;
import com.example.test_rest.models.Word;
import com.example.test_rest.service.Web;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ParsingController {
   // private final WordDAO wordDAO;


   /* @Autowired
    public ParsingController(WordDAO wordDAO){
        this.wordDAO = wordDAO;
    }*/
    @Autowired
    private Web web;

    @GetMapping()
    public WebPage newString(@RequestParam(required = false) String name){

        //name = "https://www.simbirsoft.com/";
        //name = "https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D1%88%D0%BA%D0%B0";
        web.setNameWeb(name);
        //wordDAO.save(web.getListBody());
        //List list = wordDAO.allWords();
        return web.getWebPage();
    }

}
