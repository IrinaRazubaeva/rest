package com.example.test_rest.dao;

import com.example.test_rest.entity.WebPage;
import com.example.test_rest.entity.WebWord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WordDAO extends CrudRepository<WebPage, Integer> {

    WebPage findByUrl(String url);

}
