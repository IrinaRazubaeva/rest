package com.example.test_rest.models;

import javax.persistence.*;


public class Word {

    private int id;

    private String name;

    private int cnt;

    public Word(){

    }
    public Word(String name, int cnt){
        //this.id = id;
        this.name = name;
        this.cnt = cnt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }
}
