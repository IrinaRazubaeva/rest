package com.example.test_rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.*;
import java.sql.DriverManager;
import java.sql.SQLException;

@SpringBootApplication
public class TestRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestRestApplication.class, args);

    }

}
